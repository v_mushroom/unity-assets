# Low poly articulated motorbike assets

![Bikes](/Docs/bikes.jpg)

- Banana bike
- Dirt bike with two skins
- Chopper bike

![Articulation](/Docs/articulation.gif)

> Articulation of front and reer suspension

## Usage

![Setup](/Docs/setup.jpg)

1. Use your own code to assign `.position` and `.rotation` from your wheel colliders to the reference transforms, as you would to wheel shapes.
2. The code on the bike prefab applies steering to the model, to match the rotation of the front wheel reference.
3. The code on the prefab also applies the spin rotation of the reference transforms to the wheel models.

![Steering](/Docs/steering.jpg)

> Steering following the steer parameter of the wheel collider

The wheel models are located just inside the prefabs, for easier access.
If you want to use your own wheels, replace the references on the `BikeRepresentation` component of the bike prefab.

The code on the prefabs moves the articulated pieces, like suspensions and wheel forks, to approximately match the vertical movement of the reference transforms.
The movement is clamped to the values configured on the front and rear suspension components.
If you need your wheel colliders to move more than what is configured on the suspension, the movement will be clamped, 
unless you change the configured values, but then you risk the wheels clipping through the bike shapes, or things looking a bit weird if the suspensions are over extended.

![Over extended](/Docs/over-extended.jpg)

> Suspension movement clamping when wheel colliders over extending


Some bikes can have multiple skins, so you can set the visibility for the bits that you want.

That is essentially all the setup that is needed. There is also an interactive, somewhat driveable working demo.