using UnityEngine;

public class ShockAbsorber : MonoBehaviour
{
    [SerializeField] Transform top;
    [SerializeField] Transform bottom;
    [SerializeField] Transform spring;
    [SerializeField] Transform springTarget;
    //private float originalDistance;
    private float originalSpringDistance;

    private void Awake()
    {
        //originalDistance = Vector3.Distance(top.position, bottom.position);
        if (springTarget == null)
        {
            springTarget = bottom;
        }
        if (spring != null)
        {
            originalSpringDistance = Vector3.Distance(spring.position, springTarget.position);
        }
    }

    private void FixedUpdate()
    {
        //UpdateParts();
    }

    public void UpdateParts()
    {
        top.LookAt(bottom.position, top.parent.up);
        bottom.LookAt(top.position, top.parent.up);
        if (spring != null)
        {
            float distance = Vector3.Distance(spring.position, springTarget.position);
            float scale = distance / originalSpringDistance;
            spring.localScale = new Vector3(1, 1, scale);
        }
    }
}
