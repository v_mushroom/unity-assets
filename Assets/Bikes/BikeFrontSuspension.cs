using UnityEngine;

public class BikeFrontSuspension : MonoBehaviour
{
    [SerializeField] public Transform wheelReference;
    [SerializeField] public Transform wheelPosition;
    [SerializeField] private Transform movingSuspension;

    [SerializeField] public float maxTravelUp;
    [SerializeField] public float maxTravelDown;


    private float wheelTravel;
    private float referenceZero;

    private void Awake()
    {
        referenceZero = wheelReference.localPosition.y;
    }

    public void UpdateWheelPosition()
    {
        wheelTravel = wheelReference.localPosition.y - referenceZero;
        wheelTravel = Mathf.Max(Mathf.Min(wheelTravel, maxTravelUp), maxTravelDown);

        movingSuspension.localPosition = new Vector3(0, wheelTravel, 0);
    }
}
