using UnityEngine;

public class RotatingArm : MonoBehaviour
{
    [SerializeField] Transform arm;
    [SerializeField] Transform target;
    [SerializeField] bool useCorrection = false;
    [SerializeField] float maxTravelUp = 0;
    [SerializeField] float maxTravelDown = 0;

    private float originalDistance;
    private float originalDistanceSq;
    private float originalTargetLocalY;

    private void Awake()
    {
        originalDistance = Vector3.Distance(arm.position, target.position);
        originalDistanceSq = originalDistance * originalDistance;
        originalTargetLocalY = target.localPosition.y;
    }

    private void FixedUpdate()
    {
        //UpdateParts();
    }

    public void UpdateParts()
    {
        arm.LookAt(target.position, arm.parent.up);

        if (useCorrection)
        {
            Debug.DrawLine(arm.position, target.position, Color.red);

            float yMin = originalTargetLocalY - maxTravelDown;
            float yMax = originalTargetLocalY + maxTravelUp;

            Vector3 correctedTarget = target.localPosition;

            if (correctedTarget.y < yMin)
            {
                correctedTarget.y = yMin;
            }
            else if (correctedTarget.y > yMax)
            {
                correctedTarget.y = yMax;
            }

            correctedTarget = target.parent.TransformPoint(correctedTarget);
            correctedTarget = arm.parent.InverseTransformPoint(correctedTarget);
            float y = correctedTarget.y - arm.localPosition.y;
            float z = Mathf.Sqrt(originalDistanceSq - y * y);
            correctedTarget.z = arm.localPosition.z + Mathf.Sign(correctedTarget.z) * z;
            correctedTarget = arm.parent.TransformPoint(correctedTarget);

            arm.LookAt(correctedTarget, arm.parent.up);

            Debug.DrawLine(arm.position, correctedTarget, Color.green);
        }
    }
}
