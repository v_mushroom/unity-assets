using UnityEngine;

public class BikeRepresentation : MonoBehaviour
{
    [SerializeField] private Transform rearWheel;
    [SerializeField] private Transform frontWheel;
    [SerializeField] public Transform rearWheelReference;
    [SerializeField] public Transform frontWheelReference;
    [SerializeField] private Transform rearWheelPosition;
    [SerializeField] private Transform frontWheelPosition;
    //[SerializeField] public BikeRearSuspension rearSuspension;
    [SerializeField] public BikeFrontSuspension frontSuspension;
    [SerializeField] private RotatingArm[] rotatingArms;
    [SerializeField] private ShockAbsorber[] shockAbsorbers;
    [SerializeField] private Transform steeringPivot;
    [SerializeField] private float maxSteeringAngle;

    private void Awake()
    {
        //if (rearWheelReference == null)
        //{
        //    rearWheelReference = rearSuspension.wheelReference;
        //}

        //if (rearWheelPosition == null)
        //{
        //    rearWheelPosition = rearSuspension.wheelPosition;
        //}
    }

    void FixedUpdate()
    {
        TurnSteering();
        //rearSuspension.UpdateWheelPosition();
        frontSuspension.UpdateWheelPosition();
        foreach (var rotatingArm in rotatingArms)
        {
            rotatingArm.UpdateParts();
        }
        foreach (var shock in shockAbsorbers)
        {
            shock.UpdateParts();
        }
        ApplyWheelPositions();
    }

    private void TurnSteering()
    {
        Vector3 right = transform.right;
        Vector3 forward = transform.forward;
        Vector3 refRight = frontSuspension.wheelReference.right;
        Vector3 projRefRight = Vector3.ProjectOnPlane(refRight, transform.up);

        //Debug.DrawRay(frontSuspension.wheelReference.position, right, Color.green);
        //Debug.DrawRay(frontSuspension.wheelReference.position, forward, Color.green);

        //Debug.DrawRay(frontSuspension.wheelReference.position, refRight, Color.yellow);
        //Debug.DrawRay(frontSuspension.wheelReference.position, projRefRight, Color.blue);

        float steerAngle = Vector3.Angle(right, projRefRight);
        float forwardAngle = Vector3.Angle(forward, projRefRight);

        if (forwardAngle < 90)
        {
            steerAngle = -steerAngle;
        }

        steerAngle = Mathf.Clamp(steerAngle, -maxSteeringAngle, maxSteeringAngle);

        steeringPivot.transform.localEulerAngles = new Vector3(0, steerAngle, 0);
    }

    private void ApplyWheelPositions()
    {
        //rearWheel.position = rearSuspension.wheelPosition.position;
        //rearWheel.eulerAngles = rearSuspension.wheelReference.eulerAngles;

        rearWheel.position = rearWheelPosition.position;
        rearWheel.eulerAngles = rearWheelReference.eulerAngles;

        frontWheel.position = frontSuspension.wheelPosition.position;

        Vector3 up = transform.up;
        Vector3 refUp = frontSuspension.wheelReference.up;

        //Debug.DrawRay(frontSuspension.wheelReference.position, up, Color.green);
        //Debug.DrawRay(frontSuspension.wheelReference.position, refUp, Color.red);

        float angle = frontSuspension.wheelReference.localEulerAngles.x;
        if (Vector3.Angle(up, refUp) > 90)
        {
            angle = 180 - angle;
        }

        frontSuspension.wheelPosition.localEulerAngles = new Vector3(angle, 0, 0);
        frontWheel.rotation = frontSuspension.wheelPosition.rotation;
    }
}
