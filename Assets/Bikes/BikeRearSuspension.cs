using System;
using UnityEngine;

public class BikeRearSuspension : MonoBehaviour
{
    [SerializeField] public Transform wheelReference;
    [SerializeField] public Transform wheelPosition;

    [SerializeField] public float maxTravelUp;
    [SerializeField] public float maxTravelDown;

    [SerializeField] private MoveableComponent[] rotatingComponents;
    [SerializeField] private MoveableComponent[] movingComponents;
    [SerializeField] private MoveableComponent[] scalingComponents;

    private float wheelTravel;
    private float referenceZero;

    private void Awake()
    {
        referenceZero = wheelReference.localPosition.y;
    }

    public void UpdateWheelPosition()
    {
        wheelTravel = wheelReference.localPosition.y - referenceZero;
        wheelTravel = Mathf.Max(Mathf.Min(wheelTravel, maxTravelUp), maxTravelDown);

        foreach (MoveableComponent component in rotatingComponents)
        {
            component.transform.localEulerAngles = new Vector3(wheelTravel * component.multiplier, 0, 0);
        }
        foreach (MoveableComponent component in movingComponents)
        {
            component.transform.localPosition = new Vector3(0, wheelTravel * component.multiplier, 0);
        }
        foreach (MoveableComponent component in scalingComponents)
        {
            component.transform.localScale = new Vector3(1, 1 + wheelTravel * component.multiplier, 1);
        }
    }

    [Serializable]
    public class MoveableComponent
    {
        public Transform transform;
        public float multiplier;
    }
}
