﻿using UnityEngine;

[System.Serializable]
public struct SegmentedTentacleControl
{
    [SerializeField] public Vector3 bend;
    [SerializeField][Range(0, 1)] public float offset;
    [SerializeField][Range(0, 1)] public float influence;
    [SerializeField] public bool active;
}
