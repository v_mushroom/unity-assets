using UnityEngine;

[ExecuteInEditMode]
public class SegmentedTentacleWithThreeControls : SegmentedTentacle
{
    [SerializeField] SegmentedTentacleControl control1;
    [SerializeField] SegmentedTentacleControl control2;
    [SerializeField] SegmentedTentacleControl control3;

    private void Awake()
    {
        //controls = new[] { control1, control2, control3 };
    }

    private void Update()
    {
        ApplyControls();
    }

    protected override void ApplyControls()
    {
        controls = new[] { control1, control2, control3 };
        base.ApplyControls();
    }
}
