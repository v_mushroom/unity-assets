using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TentacleSegment : MonoBehaviour
{

    public Transform turnSegment;
    public float speed;

    private float angle = 0;

    private void FixedUpdate()
    {
        angle += speed * Time.fixedDeltaTime;
        turnSegment.localEulerAngles = new Vector3(0, angle, 0);
    }

}
