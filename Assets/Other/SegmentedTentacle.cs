using UnityEngine;

[ExecuteInEditMode]
public class SegmentedTentacle : MonoBehaviour
{
    [SerializeField] Transform[] segments;
    [SerializeField] protected SegmentedTentacleControl[] controls;

    void Start()
    {

    }

    private void Update()
    {
        ApplyControls();
    }

    protected virtual void ApplyControls()
    {
        ResetSegments();
        foreach (var control in controls)
        {
            ApplyControl(control);
        }
    }

    private void ResetSegments()
    {
        foreach (var segment in segments)
        {
            segment.localEulerAngles = Vector3.zero;
        }
    }
    private void ApplyControl(SegmentedTentacleControl control)
    {
        if (control.influence == 0 || !control.active)
        {
            return;
        }
        for (int i = 0; i < segments.Length; i++)
        {
            float p = (i + 1.0f) / segments.Length;
            float distance = Mathf.Abs(control.offset - p);
            float effect = 1 - Mathf.Clamp(distance / control.influence, 0, 1);
            // Debug.Log(i + " : p=" + p + " d=" + distance + " e=" + effect);
            segments[i].localEulerAngles = segments[i].localEulerAngles + control.bend * effect;
        }
    }

}

