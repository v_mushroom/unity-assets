using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class DemoController : MonoBehaviour
{
    [SerializeField] CameraControl cameraControl;
    [SerializeField] BikeController[] bikes;

    private InputControls inputControls;
    private int bikeIndex = 0;

    private void Awake()
    {
        inputControls = new InputControls();
        inputControls.Default.Enable();
        inputControls.Default.Reset.performed += OnRestart;
    }

    private void OnDestroy()
    {
        inputControls.Default.Reset.performed -= OnRestart;
        inputControls.Dispose();
    }

    private void FixedUpdate()
    {
        float rotateInput = inputControls.Default.CameraRotate.ReadValue<float>();
        cameraControl.rotateOffset += rotateInput * cameraControl.rotateSpeed * Time.fixedDeltaTime;
    }

    private void OnRestart(InputAction.CallbackContext obj)
    {
        ShowNextBike();
    }

    private void ShowNextBike()
    {
        bikes[bikeIndex].gameObject.SetActive(false);
        bikeIndex = (bikeIndex + 1) % bikes.Length;
        bikes[bikeIndex].gameObject.SetActive(true);
        bikes[bikeIndex].ResetBike();

        cameraControl.target = bikes[bikeIndex].gameObject;
    }
}
