using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BikeShowControl : MonoBehaviour
{
    [SerializeField] private BikeRepresentation bike;
    [SerializeField] private GameObject rearWheelPosition;
    [SerializeField] private GameObject frontWheelPosition;
    [SerializeField] private float rearWheelTravel;
    [SerializeField] private float frontWheelTravel;
    [SerializeField] private float rearWheelTravelSpeed;
    [SerializeField] private float frontWheelTravelSpeed;
    [SerializeField] private float steerSpeed;
    [SerializeField] private float steerAngle;
    [SerializeField] private float wheelsRpm;
    [SerializeField] private float rotateSpeed;

    private float rearWheelPhase = 0;
    private float frontWheelPhase = 0;
    private float steerPhase = 0;
    private float rotate = 0;

    private Vector3 rearWheelOrigin;
    private Vector3 frontWheelOrigin;
    private float rearWheelRotation = 0;
    private float frontWheelRotation = 0;

    private void Awake()
    {
        rearWheelOrigin = rearWheelPosition.transform.localPosition;
        frontWheelOrigin = frontWheelPosition.transform.localPosition;
    }

    void Update()
    {
        rearWheelPhase += rearWheelTravelSpeed * Time.deltaTime;
        frontWheelPhase += frontWheelTravelSpeed * Time.deltaTime;
        steerPhase += steerSpeed * Time.deltaTime;
        rotate += rotateSpeed * Time.deltaTime;

        transform.eulerAngles = new Vector3(0, rotate, 0);

        ApplyRpmToWheels(wheelsRpm, wheelsRpm);
        ApplySteer();

        rearWheelPosition.transform.localPosition = rearWheelOrigin + new Vector3(0, Mathf.Sin(rearWheelPhase) * rearWheelTravel, 0);
        frontWheelPosition.transform.localPosition = frontWheelOrigin + new Vector3(0, Mathf.Sin(frontWheelPhase) * frontWheelTravel, 0);
    }

    public void ApplyRpmToWheels(float rearRpm, float frontRpm)
    {
        rearWheelRotation += RpmToRotationChange(rearRpm) * Time.deltaTime;
        frontWheelRotation += RpmToRotationChange(frontRpm) * Time.deltaTime;

        bike.rearWheelReference.localEulerAngles = new Vector3(rearWheelRotation, 0, 0);
        bike.frontSuspension.wheelReference.localEulerAngles = new Vector3(frontWheelRotation, 0, 0);
    }

    public void ApplySteer()
    {
        float steer = Mathf.Sin(steerPhase) * steerAngle;
        Transform reference = bike.frontSuspension.wheelReference;
        //reference.RoateAround(reference.position, Vector3.up, steer);
        bike.frontSuspension.wheelReference.RotateAround(reference.position, Vector3.up, steer);
    }

    private float RpmToRotationChange(float rpm)
    {
        return 360 * rpm / 60;
    }
}
