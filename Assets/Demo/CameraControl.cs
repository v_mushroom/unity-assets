using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class CameraControl : MonoBehaviour
{

    [SerializeField] public GameObject target;
    [SerializeField] Vector3 offset;
    [SerializeField] float speed = 1;
    [SerializeField] public float rotateOffset = 0;
    [SerializeField] public float rotateSpeed;

    private Vector3 currentPosition;
    private Vector3 goalPosition;
    private Vector3 lookPosition;
    private Vector3 goalLookPosition;
    private float goalY;

    private void Awake()
    {
        currentPosition = target.transform.position - offset;
        goalPosition = currentPosition;
        transform.position = currentPosition;
        lookPosition = target.transform.position;
        goalY = target.transform.eulerAngles.y;
    }

    private void Update()
    {
        //currentPosition = Vector3.Lerp(currentPosition, goalPosition, Time.deltaTime * speed);
        //lookPosition = Vector3.Lerp(lookPosition, goalLookPosition, Time.deltaTime);
        //y = Mathf.Lerp(y, goalY, Time.deltaTime * speed);

        //transform.position = currentPosition;
        //transform.eulerAngles = new Vector3(0, y, 0);
        //transform.LookAt(lookPosition);
        //UpdatePosition(Time.deltaTime);
    }

    private void FixedUpdate()
    {
        goalPosition = target.transform.position - target.transform.TransformDirection(offset);
        goalLookPosition = target.transform.position;
        goalY = target.transform.eulerAngles.y + rotateOffset;

        /*
        currentPosition = Vector3.Lerp(currentPosition, goalPosition, Time.fixedDeltaTime * speed);
        lookPosition = Vector3.Lerp(lookPosition, goalLookPosition, Time.fixedDeltaTime);

        //y = Mathf.Lerp(y, goalY, Time.fixedDeltaTime * speed);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.x, goalY, transform.rotation.z), Time.fixedDeltaTime * speed);

        transform.position = currentPosition;
        //transform.eulerAngles = new Vector3(0, y, 0);
        //transform.LookAt(lookPosition);
        */

        UpdatePosition(Time.fixedDeltaTime);
    }

    private void UpdatePosition(float deltaTime)
    {
        currentPosition = Vector3.Lerp(currentPosition, goalPosition, deltaTime * speed);
        lookPosition = Vector3.Lerp(lookPosition, goalLookPosition, deltaTime);

        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(transform.rotation.x, goalY, transform.rotation.z), deltaTime * speed);
        transform.position = currentPosition;
    }

}
