using UnityEngine;
using UnityEngine.InputSystem;

public class BikeController : MonoBehaviour
{
    [SerializeField] private WheelCollider rearWheel;
    [SerializeField] private WheelCollider rearLeftWheel;
    [SerializeField] private WheelCollider rearRightWheel;
    [SerializeField] private WheelCollider frontWheel;
    [SerializeField] private WheelCollider frontLeftWheel;
    [SerializeField] private WheelCollider frontRightWheel;
    [SerializeField] private GameObject rearWheelPosition;
    [SerializeField] private GameObject frontWheelPosition;
    [SerializeField] private GameObject centerOfMass;
    [SerializeField] private float motorTorque;
    [SerializeField] private float brakeTorque;
    [SerializeField] private float maxSteeringAngle;

    [SerializeField] private float inputRollCorrection;
    [SerializeField] private float inputRollSpeedCorrection;
    [SerializeField] private float inputRollOffset;

    //public float outRoll;
    public float outInput;
    private float outInputCorrection;
    public float outSpeed;

    private Rigidbody myRigidbody;
    private float rollSpeed;
    private float lastRoll = 0;

    private InputControls inputControls;
    private float smoothCorrection = 0;
    private float smoothSteerInput = 0;
    private float smoothCorrectedSteerInput = 0;

    private Vector3 velocity = Vector3.zero;
    private Vector3 lastPosition = Vector3.zero;
    private Vector3 lastVelocity = Vector3.zero;
    private Vector3 acceleration = Vector3.zero;

    private void Awake()
    {
        myRigidbody = GetComponent<Rigidbody>();
        if (centerOfMass != null)
        {
            myRigidbody.centerOfMass = centerOfMass.transform.localPosition;
        }
        inputControls = new InputControls();
        inputControls.Default.Enable();
        //inputControls.Default.Reset.performed += OnRestart;
    }

    private void OnDestroy()
    {
        //inputControls.Default.Reset.performed -= OnRestart;
        inputControls.Dispose();
    }

    private void FixedUpdate()
    {
        velocity = (myRigidbody.position - lastPosition) / Time.fixedDeltaTime;
        lastPosition = myRigidbody.position;
        acceleration = Vector3.Lerp(acceleration, velocity - lastVelocity, 0.5f);
        //acceleration = myRigidbody.velocity - lastVelocity;
        lastVelocity = Vector3.Lerp(lastVelocity, velocity, 0.5f);

        float throttleInput = inputControls.Default.Throttle.ReadValue<float>();
        float brakeInput = inputControls.Default.Brake.ReadValue<float>();
        float steerInput = inputControls.Default.Steer.ReadValue<float>();
        smoothSteerInput = Mathf.Lerp(smoothSteerInput, steerInput, 0.1f);

        float roll = MeasureRoll() + smoothSteerInput * inputRollOffset;
        float speed = MeasureSpeed();
        rollSpeed = roll - lastRoll;
        lastRoll = roll;

        float inputCorrection = roll * inputRollCorrection + rollSpeed * inputRollSpeedCorrection;
        if (speed < 3)
        {
            inputCorrection *= 1 + 3 * (3 - speed);
        }
        if (speed > 8) {
            float t = Mathf.Clamp((speed - 8) / 5, 0, 1);
            inputCorrection *= Mathf.Lerp(1, 2, t);
        }

        smoothCorrection = Mathf.Lerp(smoothCorrection, inputCorrection, 0.25f);
        outInputCorrection = smoothCorrection;

        float correctedSteerInput = Mathf.Clamp(-smoothSteerInput + smoothCorrection, -1, 1);//
        smoothCorrectedSteerInput = Mathf.Lerp(smoothCorrectedSteerInput, correctedSteerInput, 0.5f);

        outInput = steerInput;
        outSpeed = speed;

        float steering = maxSteeringAngle * smoothCorrectedSteerInput;

        if (rearWheel != null)
        {
            rearWheel.motorTorque = throttleInput * motorTorque;
        }

        if (frontWheel != null)
        {
            frontWheel.brakeTorque = brakeInput * brakeTorque;
            frontWheel.steerAngle = steering;
        }

        //float springLeanFactor = Mathf.Clamp(speed < 3 ? -1 : (speed - 3) / 2, -1, 1);

        float springLeanFactor = Mathf.Lerp(-1, 1, Mathf.Clamp((speed - 3) / 2, 0, 1));
        //Debug.Log(springLeanFactor);

        if (rearLeftWheel != null && rearRightWheel != null)
        {
            rearLeftWheel.motorTorque = throttleInput * motorTorque / 2;
            rearRightWheel.motorTorque = throttleInput * motorTorque / 2;

            JointSpring leftSpring = rearLeftWheel.suspensionSpring;
            JointSpring rightSpring = rearRightWheel.suspensionSpring;
            //if (speed > 3)
            //{
            //    leftSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput;
            //    rightSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput;
            //}
            //else
            //{
            //    leftSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput;
            //    rightSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput;
            //}
            leftSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput * springLeanFactor;
            rightSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput * springLeanFactor;
            rearLeftWheel.suspensionSpring = leftSpring;
            rearRightWheel.suspensionSpring = rightSpring;
        }

        if (frontLeftWheel != null && frontRightWheel != null)
        {
            frontLeftWheel.brakeTorque = brakeInput * brakeTorque / 2;
            frontRightWheel.brakeTorque = brakeInput * brakeTorque / 2;
            frontLeftWheel.steerAngle = steering;
            frontRightWheel.steerAngle = steering;

            JointSpring leftSpring = frontLeftWheel.suspensionSpring;
            JointSpring rightSpring = frontRightWheel.suspensionSpring;
            //if (speed > 3)
            //{
            //    leftSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput;
            //    rightSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput;
            //}
            //else
            //{
            //    leftSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput;
            //    rightSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput;
            //}
            leftSpring.targetPosition = 0.5f - 0.5f * smoothCorrectedSteerInput * springLeanFactor;
            rightSpring.targetPosition = 0.5f + 0.5f * smoothCorrectedSteerInput * springLeanFactor;

            frontLeftWheel.suspensionSpring = leftSpring;
            frontRightWheel.suspensionSpring = rightSpring;
        }

        Vector3 position;
        Quaternion rotation;
        Vector3 position2;
        Quaternion rotation2;

        if (rearWheel != null)
        {
            rearWheel.GetWorldPose(out position, out rotation);
            rearWheelPosition.transform.position = position;
            rearWheelPosition.transform.rotation = rotation;
        }

        if (rearLeftWheel != null && rearRightWheel != null)
        {
            rearLeftWheel.GetWorldPose(out position, out rotation);
            rearRightWheel.GetWorldPose(out position2, out rotation2);
            rearWheelPosition.transform.position = Vector3.Lerp(position, position2, 0.5f);
            rearWheelPosition.transform.rotation = Quaternion.Lerp(rotation, rotation2, 0.5f);
        }

        if (frontWheel != null)
        {
            frontWheel.GetWorldPose(out position, out rotation);
            frontWheelPosition.transform.position = position;
            frontWheelPosition.transform.rotation = rotation;
        }

        if (frontLeftWheel != null && frontRightWheel != null)
        {
            frontLeftWheel.GetWorldPose(out position, out rotation);
            frontRightWheel.GetWorldPose(out position2, out rotation2);
            frontWheelPosition.transform.position = Vector3.Lerp(position, position2, 0.5f);
            frontWheelPosition.transform.rotation = Quaternion.Lerp(rotation, rotation2, 0.5f);
        }
    }

    private float MeasureRoll()
    {
        // The up as it is felt
        Vector3 feltUp = Vector3.up;// + acceleration;

        // Debug.DrawRay(myRigidbody.transform.TransformPoint(myRigidbody.centerOfMass), feltUp + acceleration);

        var fwd = transform.forward;
        fwd.y = 0;
        fwd *= Mathf.Sign(transform.up.y);
        var right = Vector3.Cross(feltUp, fwd).normalized;
        return Vector3.Angle(right, transform.right) * Mathf.Sign(transform.right.y) / 180;
    }

    private float MeasureSpeed()
    {
        Vector3 localVelocity = transform.InverseTransformDirection(myRigidbody.velocity);
        return localVelocity.z;
    }

    private void OnRestart(InputAction.CallbackContext obj)
    {
        ResetBike();
    }

    public void ResetBike()
    {
        transform.position = Vector3.zero;
        transform.localEulerAngles = Vector3.zero;
        myRigidbody.velocity = Vector3.zero;
        myRigidbody.angularVelocity = Vector3.zero;
    }

}
